<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Initlib {
    function __construct(){
        //parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->database();
    }
    function piecemaker_xml(){
        //$CI =& get_instance();
        $xml='<?xml version="1.0" encoding="utf-8"?>
                <Piecemaker>
                  <Contents>
                    <Image Source="'.$this->CI->config->item("home_plugin").'/piecemaker/web/contents/bos_slide4.png" Title=""></Image>
                    <Image Source="'.$this->CI->config->item("home_plugin").'/piecemaker/web/contents/bos_slide1.png" Title=""></Image>
                    <Image Source="'.$this->CI->config->item("home_plugin").'/piecemaker/web/contents/bos_slide2.png" Title=""></Image>
                    <Image Source="'.$this->CI->config->item("home_plugin").'/piecemaker/web/contents/bos_slide3.png" Title=""></Image>
                    
                  </Contents>
                  <Settings
                    ImageWidth="550"
                    ImageHeight="220"
                    LoaderColor="0x333333"
                    InnerSideColor="0x222222"
                    SideShadowAlpha="0.8"
                    DropShadowAlpha="0.7"
                    DropShadowDistance="25"
                    DropShadowScale="0.95"
                    DropShadowBlurX="40"
                    DropShadowBlurY="4"
                    MenuDistanceX="20"
                    MenuDistanceY="50"
                    MenuColor1="0x999999"
                    MenuColor2="0x333333"
                    MenuColor3="0xFFFFFF"
                    ControlSize="100"
                    ControlDistance="20"
                    ControlColor1="0x222222"
                    ControlColor2="0xFFFFFF"
                    ControlAlpha="0.8"
                    ControlAlphaOver="0.95"
                    ControlsX="270"
                    ControlsY="210&#xD;&#xA;"
                    ControlsAlign="center"
                    TooltipHeight="30"
                    TooltipColor="0x222222"
                    TooltipTextY="5"
                    TooltipTextStyle="P-Italic"
                    TooltipTextColor="0xFFFFFF"
                    TooltipMarginLeft="5"
                    TooltipMarginRight="7"
                    TooltipTextSharpness="50"
                    TooltipTextThickness="-100"
                    InfoWidth="200"
                    InfoBackground="0xFFFFFF"
                    InfoBackgroundAlpha="0.95"
                    InfoMargin="15"
                    InfoSharpness="0"
                    InfoThickness="0"
                    Autoplay="10"
                    FieldOfView="45"
                    ></Settings>
                  <Transitions>
                    <Transition Pieces="9" Time="1.2" Transition="easeInOutBack" Delay="0.1" DepthOffset="300" CubeDistance="30"></Transition>
                    <Transition Pieces="15" Time="3" Transition="easeInOutElastic" Delay="0.03" DepthOffset="200" CubeDistance="10"></Transition>
                    <Transition Pieces="5" Time="1.3" Transition="easeInOutCubic" Delay="0.1" DepthOffset="500" CubeDistance="50"></Transition>
                    <Transition Pieces="9" Time="1.25" Transition="easeInOutBack" Delay="0.1" DepthOffset="900" CubeDistance="5"></Transition>
                  </Transitions>
                </Piecemaker>';
        
        $this->CI->output
                ->set_content_type('application/xml')
                ->set_output($xml);
    }
    function KodePengaduan($KdJenjang, $KdJenis, $KdKab, $TglKetahui){
        
        # process tanggal
        $tgla = explode('-', $TglKetahui);
        #echo json_encode($tgla);
        $tgla[2] = $tgla[2]-2000;
        #echo json_encode($tgla);
        $tgl = implode($tgla);
        
        #process the str
        $str = $KdJenjang . $KdJenis. "/" . $KdKab . "/" . $tgl . ".";
        
        # process no // str = 21/3206/101210. 
        
        //$dno = $this->get_datashet("SELECT MAX(KdPengaduan) as kode FROM g3n_pengaduan WHERE KdPengaduan like '".$str."%'");
        $dno=$this->CI->db->query("SELECT MAX(KdPengaduan) as kode FROM g3n_pengaduan WHERE KdPengaduan like '".$str."%'");
        
        $no = '';
        foreach($dno->result() as $row){
                if($row->kode == NULL){
                        $no = '001';
                } else {
                        $bb = explode('.',$row->kode);
                        $no = $bb[1]+1;
                        if($no < 10){ $no = '00' . $no; } 
                        else { $no = '0'.$no; } 
                }
        }
        return $str.$no;
        // return 21/3206/101210.001
    }
    
    function cek_session_admin(){
        $CI =& get_instance();
        $this->CI->load->library('session');
        if(!$this->CI->session->userdata('admin_logged_in')){
	    redirect("admin_handling/login");
        }else{
	    $_SESSION['KCFINDER']=array();
	    $_SESSION['KCFINDER']['disabled'] = false;
	    $_SESSION['KCFINDER']['uploadURL'] = base_url('media/upload/files');
	    $_SESSION['KCFINDER']['uploadDir'] = FCPATH.'media/upload/files';
        }
    }
    function cek_session_handling(){
        $CI =& get_instance();
        $this->CI->load->library('session');
        if(!$this->CI->session->userdata('handling_logged_in')){
            redirect("handling/login");	
        }
    }
    
    function strip_cdata($string){
        preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $string, $matches);
        return str_replace($matches[0], $matches[1], $string);
    }
    
    function allow_ip($ip){
        if (!in_array($_SERVER['REMOTE_ADDR'], $ip)){
            //redirect('sms/ip_error');
            echo 'not permitted';
            break;
        }
    }
    function param_url($param){
        $out='';
        foreach($param as $key => $value){
            $out.='&'.$key.'='.$value;
        }
        return '?'.substr($out,1);
    }
    function shorter($input, $length, $trim){
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }
    
        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        if(!$last_space) $last_space = $length;
        $trimmed_text = substr($input, 0, $last_space);
    
        //add ellipses (...)
        $trimmed_text .= $trim;
    
        return $trimmed_text;
    }
    
    /**
    * Character Limiter
    *
    * Limits the string based on the character count.  Preserves complete words
    * so the character count may not be exactly as specified.
    *
    * @access   public
    * @param    string
    * @param    integer
    * @param    string  the end character. Usually an ellipsis
    * @return   string
    */

    function character_limiter($str, $n = 500, $end_char = '&#8230;')
    {
	if (strlen($str) < $n)
	{
	    return '<p>'.$str.'</p>';
	}

	$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

	if (strlen($str) <= $n)
	{
	    return '<p>'.$str.'</p>';
	}

	$out = "";
	foreach (explode(' ', trim($str)) as $val)
	{
	    $out .= $val.' ';

	    if (strlen($out) >= $n)
	    {
		$out = trim($out);
		return (strlen($out) == strlen($str)) ? '<p>'.$out.'</p>' : '<p>'.$out.'</p>'.$end_char;
	    }
	}
    }
    
}

/* End of file Someclass.php */