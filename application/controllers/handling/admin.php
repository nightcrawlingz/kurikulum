<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('handling/login_db', 'admin_handling/user_handling_db'));
        $this->load->library(array('session','initlib'));
        $this->load->database();
        
	$this->initlib->cek_session_handling();
    }
    
    function index(){
	//$data['module']='pengaduan';
	//$this->load->view('admin_handling/main',$data);
	redirect('handling/pengaduan');
    }
    
    function post_ganti_password(){
	//print_r($_POST);
	$data['username'] = $this->session->userdata('user_handling')->username;
	$data['password'] = $this->input->post('password_lama');
	if($this->login_db->login_handling($data)){
	    if($this->user_handling_db->save_user_handling($this->session->userdata('user_handling')->id, array('password' => md5($this->input->post('password_baru')), 'last_update' => time()))){
	    
		$this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
	    }else{
		$this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
	    }
	
	}else{
	    $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
	}
	redirect('handling/pengaduan');
    }
    
}
 ?>
