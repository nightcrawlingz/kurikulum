<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Halaman extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->database();
        session_start();
	$this->initlib->cek_session_admin();
    }
    
    function index(){
	redirect('admin_handling/halaman/tentang');
    }
    
    function tentang($page){
        $data['module']='halaman_statis';
        $data['title'] = 'Tentang '.strtoupper($page);
        $data['statis']=$this->content_statis_db->get('about_'.$page);
        
        $this->load->view('admin_handling/main',$data);
    }
    
    function post_content_statis($id){
        $redirect = 'admin_handling/halaman/tentang/'.$id;
        $id = 'about_'.$id;
        $data_content=array(
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'last_update' => date('Y-m-d H:i:s'),
            'author' => $this->session->userdata('admin')->person_id
        );
        
        $data_content_statis=array(
            'id' => $id,
            'published' => ($this->input->post('published') ? 1 : 0)
        );
        
        //print_r($data_content);
        if($this->content_statis_db->save($id, $data_content, $data_content_statis)){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
        redirect($redirect);
    }
    
    private function content_dinamis($param='home',$id=null, $data){
	if($param=='add' || $param=='edit'){
	    $data['module']='halaman_dinamis_add';
	    $data['dinamis']=$this->content_dinamis_db->get($id);
	    
	    //print_r($data['dinamis']); exit;
	    //print_r($data['dinamis']);
	    $data['id']=$id;
	    $this->load->view('admin_handling/main',$data);    
	}elseif($param=='home'){
	    $data['module']='halaman_dinamis';
	    $data['load_url']=site_url('admin_handling/halaman/ajax_content_dinamis/'.$data['content_kategori_id']);
	    $this->load->view('admin_handling/main',$data);
	}elseif($param=='delete'){
	    $this->content_dinamis_db->delete($id);
	    redirect('admin_handling/halaman/'.$this->uri->segment(3));
	}
    }
    /*
    function berita($param='home',$id=null){
        $data['title']='Berita';
	$data['content_kategori_id']=1;
	
	$this->content_dinamis($param, $id, $data);
    }*/
    
    function pengumuman($param='home',$id=null){
        $data['title']='Pengumuman';
	$data['content_kategori_id']=2;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function peraturan($param='home',$id=null){
        $data['title']='Peraturan Perundangan';
	$data['content_kategori_id']=3;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function publikasi($param='home',$id=null){
        $data['title']='Publikasi';
	$data['content_kategori_id']=4;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function agenda($param='home',$id=null){
        $data['title']='Agenda';
	$data['content_kategori_id']=5;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function referensi_buku($param='home',$id=null){
        $data['title']='Usulan Buku';
	$data['content_kategori_id']=7;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function referensi_standar_pendidikan($param='home',$id=null){
        $data['title']='Usulan Standar Pendidikan';
	$data['content_kategori_id']=8;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function referensi_kerangka_dasar_dan_struktur($param='home',$id=null){
        $data['title']='Usulan Kerangka Dasar dan Struktur';
	$data['content_kategori_id']=9;
	
	$this->content_dinamis($param, $id, $data);
    }
    
    function ajax_content_dinamis($content_kategori_id){
        $this->datatables->select('
            content_dinamis.id as id,
            content.title,
            content.last_update as tanggal,
            person.nama as author,
            if(content_dinamis.published=1, "ya", "tidak") as published,
            content_dinamis.id as action
        ',false);
        $this->datatables->from('content_dinamis');
        $this->datatables->join('content','content.id = content_dinamis.content_id','inner');
        $this->datatables->join('person','content.author = person.id', 'inner');
        
        $this->datatables->where('content_dinamis.content_kategori_id', $content_kategori_id);
	$this->datatables->where('content_dinamis.deleted',0);
        $this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
	
	$kategori=array( 1 => 'berita', 2 => 'pengumuman', 3 => 'peraturan', 4 => 'publikasi', 5 => 'agenda', 7 => 'referensi_buku', 8 => 'referensi_standar_pendidikan', 9 => 'referensi_kerangka_dasar_dan_struktur');
	
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<a class="btn btn-default" href="$2/$1">Edit</a>
		<button type="button" class="btn btn-default btn_delete" data-href="$3/$1" data-toggle="modal" data-target="#modal_delete">Hapus</button>
	    </div>',
	    'id, site_url("admin_handling/halaman/'.$kategori[$content_kategori_id].'/edit"), site_url("admin_handling/halaman/'.$kategori[$content_kategori_id].'/delete")'
	);    
	
	
        
        echo $this->datatables->generate();
        //echo $this->db->last_query();
    }
    
    function post_content_dinamis($content_kategori_id, $id=null){
	//print_r($_POST);
	
	$agenda_from = $this->input->post('agenda_from');
	$agenda_to = $this->input->post('agenda_to');
	
	
	$data_content=array(
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'last_update' => date('Y-m-d H:i:s'),
            'author' => $this->session->userdata('admin')->person_id,
            'tags' => strtolower($this->input->post('tags')),
            'category' => ($this->input->post('category') ? implode(',', $this->input->post('category')) : ''),
            'agenda_from' => ($agenda_from ? date_to_mysqldatetime($agenda_from.':00') : '0000-00-00 00:00:00'),
            'agenda_to' => ($agenda_to ? date_to_mysqldatetime($agenda_to.':00') : '0000-00-00 00:00:00')
            
        );
        //print_r($data_content);
        //exit;
        $data_files=null;
        if($this->input->post('name_link'))
        $data_files=array(
	    'name' => $this->input->post('name_link'),
	    'file' => $this->input->post('name_file'),
	    'size' => $this->input->post('size_file')
        );
        
        $data_content_dinamis=array(
            'published' => ($this->input->post('published') ? 1 : 0),
	    'content_kategori_id' => $content_kategori_id
        );
        
	if($id)
	    $data_content_dinamis['id']=$id;
	
	//print_r($data_content);
	//print_r($data_content_dinamis);
	
        if($this->content_dinamis_db->save($id, $data_content, $data_files, $data_content_dinamis)){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        //echo $this->db->last_query();
        redirect($this->session->userdata('redirect'));
        
    }
    
    function post_upload(){
	error_reporting(0);
	if($this->input->server('REQUEST_METHOD')=='DELETE'){
	    $this->content_files_db->delete($this->input->get('id'));
	}
	$param=array(
	  'upload_url' => base_url('/media/upload/files/').'/',
	  'upload_dir' => FCPATH.'media/upload/files/',
	  'script_url' => site_url('admin_handling/halaman/post_upload')
	);
	$this->load->library("UploadHandler",$param);
    }
    
    function tentang_kurikulum(){
	$data['module']='info_statis';
        $data['title'] = 'Tentang Kurikulum';
        $data['statis']= $this->static_pages_db->get('tentang_kurikulum');
        
        $this->load->view('admin_handling/main',$data);
    }
    function tentang_kurikulum_post(){
	
	if($this->static_pages_db->save('tentang_kurikulum', $this->input->post())){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect($this->session->userdata('redirect'));
    }
    
    function perkembangan_kurikulum(){
	$data['module']='info_statis';
        $data['title'] = 'Perkembangan Kurikulum';
        $data['statis']= $this->static_pages_db->get('perkembangan_kurikulum');
        
        $this->load->view('admin_handling/main',$data);
    }
    function perkembangan_kurikulum_post(){
	
	if($this->static_pages_db->save('perkembangan_kurikulum', $this->input->post())){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect($this->session->userdata('redirect'));
    }
    
    function usulan(){
	$data['module']='info_statis';
        $data['title'] = 'Masukan';
        $data['statis']= $this->static_pages_db->get('masukan');
        
        $this->load->view('admin_handling/main',$data);
    }
    
    function usulan_post(){
	
	if($this->static_pages_db->save('masukan', $this->input->post())){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
	redirect($this->session->userdata('redirect'));
    }
    
    function berita_kurikulum($param='home', $id=null){

	$data['title'] = 'Berita Kurikulum';
	if($param=='add' || $param=='edit'){
	    $data['module'] = 'info_dinamis_add';
	    
	    $data['id']=$id;
 
	}elseif($param=='home'){
	    $data['module']='info_dinamis';
	    $data['dinamis']= $this->dynamic_pages_db->get_all(array('page_id' => 'berita_kurikulum'));
	    
	}elseif($param=='delete'){
	    $this->dynamic_pages_db->delete($id);
	    redirect('admin_handling/halaman/'.$this->uri->segment(3));
	}
	
        $this->load->view('admin_handling/main',$data);
    }
    
    function berita($param='home', $id=null){

	$data['title'] = 'Berita';
	if($param=='add' || $param=='edit'){
	    $data['module'] = 'info_dinamis_add';
	    
	    $data['id']=$id;
 
	}elseif($param=='home'){
	    $data['module']='info_dinamis';
	    $data['dinamis']= $this->dynamic_pages_db->get_all(array('page_id' => 'berita'));
	    
	}elseif($param=='delete'){
	    $this->dynamic_pages_db->delete($id);
	    redirect('admin_handling/halaman/'.$this->uri->segment(3));
	}
	
        $this->load->view('admin_handling/main',$data);
    }
    
    function post_info_dinamis($page_id, $id=null){
	
	$input = $this->input->post('content');
	
	$input['page_id'] = $page_id;
	if(!isset($input['published'])){
	    $input['published'] = 0;
	}
	
	
	$input['author'] = array(
	    'fullname' => $this->session->userdata('admin')['fullname'],
	    'user_id' => $this->session->userdata('admin')['user_id'],
	);
	
	if($this->dynamic_pages_db->save($id, $input)){
	    //echo $this->db->last_query();
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        //echo $this->db->last_query();
        redirect($this->session->userdata('redirect'));
        
    }
}
 ?>
