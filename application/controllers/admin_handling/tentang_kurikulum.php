<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tentang_kurikulum extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        
        $this->load->database();
        session_start();
	$this->initlib->cek_session_admin();
    }
    
    function index(){
	$data['module']='info_statis';
        $data['title'] = 'Tentang Kurikulum';
        $data['statis']= $this->static_pages_db->get('tentang_kurikulum');
        
        $this->load->view('admin_handling/main',$data);
    }
    
    function post(){
	
	if($this->static_pages_db->save('tentang_kurikulum', $this->input->post())){
// 	    print_r($this->input->post());
	
	}
	
	redirect('admin_handling/tentang_kurikulum');
    }
    
   
}
 ?>
