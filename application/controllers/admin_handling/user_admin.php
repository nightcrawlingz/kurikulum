<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->initlib->cek_session_admin();
    }
    
    function index(){
	$data['title'] = 'User Admin';
	$data['module'] = 'user_admin';
	$data['load_url'] = site_url('admin_handling/user_admin/ajax_user_admin');
	$this->load->view('admin_handling/main',$data);
    }
    
    function add(){
	$data['title'] = 'User Admin Add';
	$data['module'] = 'user_admin_add';
	$data['id'] = null;
	$data['user_handling'] = $this->user_admin_db->get($data['id']);
	$this->load->view('admin_handling/main',$data);
    }
    
    function edit($id=null){
	$data['title'] = 'User Admin Edit';
	$data['module'] = 'user_admin_add';
	$data['user_id'] = $id;
	$this->load->view('admin_handling/main',$data);
    }
    function post_add($id=null){
	$input = $this->input->post('user');
	
	if($input['password'] != ''){
	    $input['password'] = sha1_salt($input['password']);
	}else{
	    unset($input['password']);
	}
	if(!isset($input['activated'])){
	    $input['activated'] = 0;
	}
	
	if($this->user_db->save($id, $input)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
        
        redirect('admin_handling/user_admin');
    }
    function ajax_user_admin(){
	$this->datatables->select('
	    a.id as id,
	    a.username,
	    c.name as role_level,
	    if(a.activated = 1,"ya","tidak") as aktif,
	    a.id as action
	',false);
	$this->datatables->from('user a');
	$this->datatables->join('role_level_admin b','a.id = b.user_id','left');
	$this->datatables->join('level_admin c', 'c.id = b.level_id', 'left');
	
	$this->datatables->where('a.deleted = 0');
	$this->datatables->group_by('a.id','desc');
	
	
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<a href="$2/$1" class="btn btn-default" >Edit</a>
		<button type="button" class="btn btn-default btn_delete" data-href="$3/$1" data-toggle="modal" data-target="#modal_delete">Hapus</button>
	    </div>',
	    'id, site_url("admin_handling/user_admin/edit"), site_url("admin_handling/user_admin/delete")'
	);
	
	echo $this->datatables->generate();
	//echo $this->db->last_query();
    }
    
    function delete($id){
	
	if($this->user_db->delete($id)){
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	
	redirect('admin_handling/user_admin');
    }
}
 ?>
