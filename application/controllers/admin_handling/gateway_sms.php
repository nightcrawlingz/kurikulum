<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gateway_sms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('admin_handling/gateway_sms_db','admin_handling/pengaduan_db'));
        $this->load->library(array('session','initlib'));
        $this->load->database();
        
	//$this->initlib->cek_session_admin();
	$this->program_id = 4; //kur 2013
	$this->level_id = 1; //pusat
    }
    
    function index(){
	$param = $this->input->post('param');
	//print_r($param);
	if($param){
	    $data_sms = json_decode($param,true);
	    //print_r($data_sms);
	    $id = null;
	    $data_sms['tanggal'] = date('Y-m-d H:i:s');
	    if($this->gateway_sms_db->save($id,$data_sms)){
		echo 'OK';
	    }else{
		echo 'NOK';
	    }
	    
	}
	
    }
    
    function post_pengaduan(){
    
	$param = $this->input->post('param');
	//print_r($param);
	if($param){
	    //ada isinya
	    
	    $data_sms = json_decode($param,true);
	
	    $data_person=array(
		'handphone' => $data_sms['msisdn'],
		'operator_id' => $data_sms['operator_id']
	    );
	    
	    $data_pengaduan=array(
		'program_id' => $this->program_id,
		'level_id' => $this->level_id,
		'tanggal' => (isset($data_sms['tanggal']) ? $data_sms['tanggal'] : date('Y-m-d H:i:s')),
		'deskripsi' => htmlentities($data_sms['pesan']),
		'last_update' => date('Y-m-d H:i:s'),
		'media_id' => 2
	    );
	    
	    $data_param_pengaduan = array(
		'tampil_nama' => 0,
		'tampil_telp' => 0,
		'tampil_alamat' => 0
	    );
	    
	    if($this->pengaduan_db->save($id=null,$data_person, $data_pengaduan, $data_param_pengaduan, $data_isu = array() )){
		echo 'OK';
	    }else{
		echo 'NOK';
	    }
	}
    }
    
}
 ?>
