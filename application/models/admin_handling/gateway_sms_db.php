<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gateway_sms_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
	return $this->db->get_where('pesan_sms',  array('id' => $id));
    }
    
    function get($id){
	return $this->get_all(array('id' => $id));
    }
    
    function get_all($filter=array(), $limit=null, $offset=0, $deleted = 0){
	$this->db->select('a.id, a.operator_id, b.name as operator, a.msisdn, a.pesan, a.tanggal');
	$this->db->from('pesan_sms as a');
	$this->db->join('operator as b','a.operator_id = b.id','left');
	
	
	if($filter){
	    if(isset($filter['id']))
		$this->db->where('a.id', $filter['id']);
	}
	
	
	if($deleted)
	    $this->db->where('a.deleted',1);
	else
	    $this->db->where('a.deleted',0);
	    
	if($limit)
	    $this->db->limit($limit, $offset);
	
	//$this->db->order_by('a.id');
	return $this->db->get();
    }
    
    function save(&$id, $data_sms){
	$result=false;
        $this->db->trans_start();
        
	$exist=$this->exist($id);
	if($exist->num_rows() == 1){
	    //update
	    $sms = $exist->row();
	    
	    //save pesan_sms
	    $this->db->where('id', $id);
	    $result = $this->db->update('pesan_sms', $data_sms);
	    
	}else{
	    //insert
	    //save person
	    $result = $this->db->insert('pesan_sms', $data_sms);
	}
	
	$this->db->trans_complete();
	
	return $result;
    }
    
}
?>