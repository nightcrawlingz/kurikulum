<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_files_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function save($content_id=null, $data_files){
	if($content_id)
	    $this->db->delete('content_files', array('content_id' => $content_id));
	
	for($i=0; $i<count($data_files['name']); $i++){
	    $data[]=array(
		'content_id' => $content_id,
		'name' => $data_files['name'][$i],
		'file' => $data_files['file'][$i],
		'size' => $data_files['size'][$i]
	    );
	}
	
	return $this->db->insert_batch('content_files', $data);
    }
    function get($id){
	$this->db->select('content_files.id, content_files.name, content_files.file, content_files.size');
        $this->db->from('content_dinamis');
        $this->db->join('content', 'content.id = content_dinamis.content_id','inner');
        $this->db->join('content_files', 'content_files.content_id = content.id','inner');
        
        $this->db->where('content_dinamis.id',$id);
        
        $this->db->where('content_dinamis.deleted', 0);
        
        return $this->db->get();
    }
    function delete($id){
	return $this->db->delete('content_files', array('id' => $id));
    }
}
?>