<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Participant_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($participant_id){
	return count($this->get_all(array('participant_id' => $participant_id))) == 1;
    }
    
    function get($participant_id){
	return $this->get_all($filter = array('participant_id' => $participant_id));
    }
    
    function get_all($filter=null){
	
	if(isset($filter['participant_id']))
	    $this->mongoci->where('_id', new MongoId($filter['participant_id']));
	
	$this->mongoci->where('deleted',0);
        $this->mongoci->orderBy(array('created' => 'DESC'));
        
        return $this->mongoci->get('participant');
        //$this->mongoci->lastQuery();
    }
    
    function save($participant_id=null, $data_page=array(), $data_push=array()){
	$result = false;
	
	$last_update = array('time' => time(), 'ip' => $this->input->ip_address());
	
	if($participant_id && $this->exist($participant_id)){
	    //update
	    $this->mongoci->where('_id', new MongoId($participant_id));
	    
	    if($data_page)
		$this->mongoci->set($data_page);
	    
	    $this->mongoci->set('last_update', $last_update);
	    $this->mongoci->push('log_last_update', $last_update);
	    
	    if($data_push)
		$this->mongoci->push($data_push);

	    $result = $this->mongoci->update('participant');
	    
	}else{
	    //insert
	    $data_page['last_update'] = $last_update;
	    $data_page['log_last_update'] = array($last_update);
	    $data_page['created'] = time();
	    $data_page['deleted'] = 0;
	    
	    $result = $this->mongoci->insert('participant', $data_page);
	    
	}
	
	return $result;
    }
    
    function delete($participant_id=null){
	return $this->save($participant_id, array('deleted' => 1));
    }
    
}
?> 
