<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Init_config_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function get(){
	$query = $this->mongoci->get('init_config');
	return $query[0];
    }
    function get_kategori(){
	return $this->get()['kategori'];
    }
}
?>