            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <?php
                                $notif=$this->session->flashdata('success');
                                if($notif):
                            ?>
                            <div class="alert <?=($notif['status'] ? 'alert-success' : 'alert-danger')?>"><?=$notif['msg']?></div>
                            <?php endif;?>
                           <form role="form" action='<?=site_url('admin_handling/halaman/post_content_statis/'.$this->uri->segment(2))?>' method='post'>
                            
                            <div class="form-group">
                              <label for="">Isi</label>
                              <textarea class='form-control ckeditor' name='content' placeholder='Isi'></textarea>
                            </div>
                            
                            
                            <button type="submit" class="btn btn-default">Submit</button>
                          </form>
                        </div>
                    </div>
                </div>
            </div>