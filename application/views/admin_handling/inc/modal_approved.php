	      <table class='table'>
		  <tbody>
		      <tr>
			  <td><label>Ditujukan Kepada</label></td>
			  <td>Tim <?=ucfirst($pengaduan->level)?></td>
		      </tr>
		      <tr>
			  <td><label>Daerah</label></td>
			  <td><?=($pengaduan->provinsi ? $pengaduan->provinsi.', '.$pengaduan->kabkota.', '.$pengaduan->kecamatan : '<i>kosong</i>')?></td>
		      </tr>
		      <tr>
			  <td><label>Lokasi</label></td>
			  <?php if($pengaduan->lokasi_id):?>
			      <td><?=($pengaduan->lokasi_id == 1 ? $pengaduan->jenjang.' '.ucfirst($pengaduan->status_sekolah).', '.$pengaduan->nama_lokasi : $pengaduan->nama_lokasi)?></td>
			  
			  <?php else:?>
			      <td><i>kosong</i></td>
			  
			  <?php endif;?>
		      </tr>
		      <tr>
			  <td><label>Kategori</label></td>
			  <td><?=$pengaduan->kategori?></td>
		      </tr>
		      <tr>
			  <td><label>Profesi</label></td>
			  <td><?=($pengaduan->sumber_id == 9999 ? $pengaduan->sumber.'('.$pengaduan->sumber_lain.')' : $pengaduan->sumber)?></td>
		      </tr>
		      <tr>
			  <td><label>Nama</label></td>
			  <td><?=$pengaduan->nama?> (Tampilkan : <?=($param['tampil_nama']==1 ? 'Ya' : 'Tidak')?>)</td>
		      </tr>
		      <tr>
			  <td><label>Email</label></td>
			  <td><?=$pengaduan->email?></td>
		      </tr>
		      <tr>
			  <td><label>Telp</label></td>
			  <td><?=$pengaduan->handphone?> (Tampilkan : <?=($param['tampil_telp']==1 ? 'Ya' : 'Tidak')?>)</td>
		      </tr>
		      <tr>
			  <td><label>Alamat</label></td>
			  <td><?=$pengaduan->alamat_rumah?> (Tampilkan : <?=($param['tampil_alamat']==1 ? 'Ya' : 'Tidak')?>)</td>
		      </tr>
		      <tr>
			  <td><label>Deskripsi</label></td>
			  <td><?=$pengaduan->deskripsi?></td>
		      </tr>
		       <tr>
			  <td><label>Attachment</label></td>
			  <td><?=($pengaduan->attachment ? '<a target="_blank" href="'.site_url('media/upload/files/usulan/'.$pengaduan->attachment).'">'.$pengaduan->attachment.'</a>' : '')?></td>
		      </tr>
		      <tr>
			  <td>&nbsp</td>
			  <td><input type='checkbox' name='published' value='1' id='published'> Publikasi</td>
		      </tr>
		  </tbody>
	      </table>