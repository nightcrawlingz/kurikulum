    <!-- Bootstrap -->
    <link href="<?=$this->config->item('handling_plugin')?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/css/datatables.css">
    <link id="bsdp-css" href="<?=$this->config->item('handling_plugin')?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$this->config->item('handling_css')?>/main.css">
    <link rel="shortcut icon" href="<?=$this->config->item('home_img')?>/logo_kurikulum_2013.png">
    
    <style>
.modal-lg {
    width: 85%; /* respsonsive width */

}
    .navbar {
    background: #003a6a;
    
    min-height: 90px;
    border-bottom-color: #003a6a;
     border-bottom-width: 15px;
    }
    
/* Sticky footer styles
-------------------------------------------------- */

html,
body {
  height: 100%;
  /* The html and body elements cannot have any padding or margin. */
}

/* Wrapper for page content to push down footer */
#wrap {
  min-height: 100%;
  height: auto !important;
  height: 100%;
  /* Negative indent footer by its height */
  margin: 0 auto -60px;
  /* Pad bottom by footer height */
  padding: 0 0 60px;
}

/* Set the fixed height of the footer here */
#footer {
  height: 60px;
  background-color: #f5f5f5;
}
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=$this->config->item('handling_js')?>/html5shiv.js"></script>
      <script src="<?=$this->config->item('handling_js')?>/respond.min.js"></script>
    <![endif]-->
    