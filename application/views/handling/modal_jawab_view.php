
        
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Respon
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <!--jawab-->
        <?php include('form_jawab_view.php')?>
        
        <!--end jawab-->
      
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Daftar Respon
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Username / Nama / Level</th>
              <th>Tgl Progress</th>
              <th>Uraian</th>
              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=0; foreach($respon->result() as $row):?>
            <tr>
              <td><?=++$i?></td>
              <?php
		  //echo $row->user_handling_id;
		  $role_level = $this->user_handling_db->get_role_level($row->user_handling_id)->row();
		  //echo $this->db->last_query();
		  //print_r($role_level);
		  $level = $this->select_db->level(array('id' => $role_level->level_id))->row();
		  $provinsi = '';
		  $kabkota = '';
		  $wilayah = '';
		  if($role_level->level_id == 2){ //level prov
		      $provinsi = $this->region_db->provinsi(array('id' => $role_level->provinsi_id))->row();
		      $wilayah = '('.$provinsi->name.')';
		  }elseif($role_level->level_id == 3){//level kabkota
		      $provinsi = $this->region_db->provinsi(array('id' => $role_level->provinsi_id))->row();
		      
		      $kabkota = $this->region_db->kabkota(array('id' => $role_level->kabkota_id))->row();
		      
		      $wilayah = '('.$provinsi->name.', '.$kabkota->name.')';
		  }
              ?>
              <td><?=$row->username?> / <?=$row->user?> / <?=$level->name.' '.($wilayah != '' ? $wilayah : '')?></td>
              <td><?=mysqldatetime_to_date($row->tanggal,"d/m/Y, H:i:s")?></td>
              <td><?=$row->deskripsi?></td>
              
              <td>
		  <button type="button" data-load='<?=site_url('handling/pengaduan/form_jawab/'.base64_encode($row->id))?>' data-href='<?=site_url('handling/pengaduan/post_respon/'.base64_encode($row->pengaduan_id).'/'.base64_encode($row->id))?>' class="respon-edit btn btn-default">Edit</button>
		  <button type="button" data-toggle='modal' data-target='#modal_delete' data-title='Hapus' data-body='Apakah anda yakin menghapus respon ini?' data-href='<?=site_url('handling/pengaduan/post_delete_respon/'.base64_encode($row->id))?>' class="respon-delete btn btn-default">Hapus</button>
              </td>
            </tr>
            <?php endforeach;?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>