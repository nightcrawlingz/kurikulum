	      <div class="widget-item">
                    <div class="request-information">
                        <h4 class="widget-title">Statistik Pengaduan</h4>
                        <form class="request-info clearfix" method='post' action='<?=site_url('statistik_pengaduan/post_report')?>'> 
                            <div class="full-row">
                                <label for="cat">Pengaduan Berdasarkan:</label>                
                                <div class="input-select">
                                    <select name="kategori" id="report_pengaduan" class="postform">
                                        
                                        <?php foreach($this->select_db->report_kategori()->result() as $row):?>
                                        <option value="<?=$row->id?>" <?=($row->id == $this->uri->segment(3) ? 'selected' : '')?>><?=$row->name?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div> <!-- /.input-select -->  
                            </div> <!-- /.full-row -->
                            
                            <div class="full-row">
                                <label for="cat2">Provinsi:</label>                                               
                                <div class="input-select">
                                    <select name="provinsi" id="provinsi" class="postform">
                                        <option value="all">-- semua --</option>
                                        <?php foreach($this->region_db->provinsi()->result() as $row):?>
                                        <option value="<?=$row->id?>" <?=($row->id == $this->uri->segment(5) ? 'selected' : '')?>><?=$row->name?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div> <!-- /.input-select -->
                            </div> <!-- /.full-row -->
                            
                            <div class="full-row">
                                <label for="cat2">Kabupaten / Kota:</label>                                               
                                <div class="input-select">
                                    <select name="kabkota" id="kabkota" class="postform">
                                        <option value="all">-- semua --</option>
                                        <?php foreach($this->region_db->kabkota(array('provinsi_id' => $this->uri->segment(5)))->result() as $row):?>
                                        <option value="<?=$row->id?>" <?=($row->id == $this->uri->segment(6) ? 'selected' : '')?>><?=$row->name?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div> <!-- /.input-select -->
                            </div> <!-- /.full-row -->

                            <div class="full-row">
                                <label for="cat2">Tahun:</label>                                               
                                <div class="input-select">
                                    <select name="tahun" id="tahun" class="postform">
                                        <option value="all">-- semua --</option>
                                        <?php for($tahun=2014; $tahun<=date('Y'); $tahun++):?>
                                        <option value="<?=$tahun?>" <?=($tahun == $this->uri->segment('4') ? 'selected' : '')?>><?=$tahun?></option>
                                        <?php endfor;?>
                                    </select>
                                </div> <!-- /.input-select -->
                            </div> <!-- /.full-row -->

                            <div class="full-row">
                                <div class="submit_field">
                                    
                                    <input class="mainBtn pull-right" type="submit" name="" value="Proses">
                                </div> <!-- /.submit-field -->
                            </div> <!-- /.full-row -->


                        </form> <!-- /.request-info -->
                    </div> <!-- /.request-information -->
                </div> <!-- /.widget-item -->