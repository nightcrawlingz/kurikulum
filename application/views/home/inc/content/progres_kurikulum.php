    
    <!-- Being Page Title -->
<style>
.text-center{
	text-align: center;
}

.image-container{
	text-align: center;
}

.background-white{
	background-color:#ffffff;
}

.box-container{
	width:340px;
	overflow:hidden;
}

.text-description-container{
	padding-top:10px;
	padding-bottom:10px;
    padding-right: 17%;
    padding-left: 17%;
}


</style>
	
 <div class="row" style='background:white'>
    <div class="col-md-1">
    </div>
    <div class="col-md-10">
    
        <?php
	    $query = $this->static_pages_db->get('tentang_kurikulum');
	    $content = '';
	    if($query){
		$content = $query[0]['content'];
	    }
	?>
	<?=($content ? $content : '')?>
	<div class="row  background-white">
			<div class="col-md-4 col-md-offset-2">
				<div class="box-container">
					<img src="<?php echo base_url();?>media/home/images/pencil.jpg" style="float:left"/>	
					<h4>Sudah sejauh mana<br />Kurikulum 2013 <br />Kini dikembangkan?</h4>
					<a href='<?=site_url('perkembangan_kurikulum')?>' class="btn btn-primary">Cari tahu lebih banyak</a>
				</div>
			</div>	
			<div class="col-md-6">
				<img src="<?php echo base_url();?>media/home/images/vertical_line.jpg" style="float:left"/>	
				<h4>Dokumen Kurikulum<br />Pendidikan di Indonesia</h4>
				<div class="button-container">
					<p><a target='_blank' href='http://puskurbuk.net/web13/bahan-kebijakan-kurikulum-2013.html' class="btn btn-primary">Dokumen Kurikulum</a></p>
					<p><a target='_blank' href='http://bse.kemdikbud.go.id' class="btn btn-primary">Buku Sekolah Elektronik</a></p>
				</div>
			</div>
	</div>
	
    </div>
    <div class="col-md-1">
    </div>
</div>
    
