    <?php
	$halaman = $this->uri->segment(1);
    ?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
		    <?php
			$list_halaman = array(
			    'berita' => 'Berita',
			    'agenda' => 'Agenda',
			    'usulan_buku' => 'Usulan Buku',
			    'usulan_standar_pendidikan' => 'Usulan Standar Pendidikan',
			    'usulan_kerangka_dasar_dan_struktur' => 'Usulan Kerangka Dasar dan Struktur'
			);
		    ?>
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <?=(in_array($halaman, array('usulan_buku', 'usulan_standar_pendidikan', 'usulan_kerangka_dasar_dan_struktur')) ? '<h6><a href="#">Usulan</a></h6>' : '')?>
                    <h6><span class="page-active"><?=$list_halaman[$halaman]?></span></h6>
                    
                    
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">
                <div class="row" id="list_content_halaman">
		    <?php 
			$param = array(
			    'halaman' => $halaman
			);
			$this->load->view('home/inc/content/list_content_halaman',$param)?>
                </div> <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="load-more-btn">
                            <button type="button" class="btn btn-primary btn-lg btn-block" id='loadmore'>
			      Lanjutkan
			    </button>
                        </div>
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

            </div> <!-- /.col-md-8 -->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">
		<?php 
		    $param = array(
			'halaman' => $halaman
		    );
		    $this->load->view('home/inc/sidebar_widget',$param);
		?>
            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
