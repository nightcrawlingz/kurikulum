    <div class="row">
            <div class="col-md-12">
                <div class="main-slideshow" style='background-color:transparent'>
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
				<a href='<?=site_url('progres_kurikulum')?>'>
                                <img src="<?=$this->config->item('home_img').'/slide_1_text.jpg'?>" class="thumbnail" alt="Slide 1"/>
                                </a>
                                <!--<div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>-->
                                
                            </li>
                            <li>
				<a href='<?=site_url('pengaduan')?>'>
                                <img src="<?=$this->config->item('home_img').'/slide_2_text.jpg'?>" class="thumbnail" alt="Slide 2"/>
                                </a>
                                <!--<div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>-->
                                
                            </li>
                            <li>
				<a href='<?=site_url('usulan')?>'>
                                <img src="<?=$this->config->item('home_img').'/slide_3_text.jpg'?>" class="thumbnail" alt="Slide 3"/>
                                </a>
                                <!--<div class="slider-caption">
                                    <h2><a href="blog-single.html">teks slider 1..</a></p>
                                </div>-->
                                
                            </li>
                            
                        </ul> <!-- /.slides -->
                    </div> <!-- /.flexslider -->
                </div> <!-- /.main-slideshow -->
            </div> <!-- /.col-md-12 -->
            
            
        </div>


    
    <div class='site-footer'>
	<table width='100%'>
	    <tr>
		<td width='33%' style='background-color:#41a7d8' valign="top">
		    <style>
		    .site-footer a{
			color:black;
		    }
		    </style>
		    <div class="footer-widget" style='margin-left:30px; color:black; margin-top:10px'>
			<h4 class="footer-widget-title">Hubungi Kami</h4>
			<p><strong>Kementerian Pendidikan dan Kebudayaan<br>Jalan Jenderal Sudirman Senayan Jakarta 10270.</strong>
			<ul class="list-links">
			    <li><i class="fa fa-phone"></i> Call center : <a href="tel:0215725980">021 5725980</a></li>
			    <li><i class="fa fa-phone"></i> Call center : <a href="tel:177">177</a></li>
			    <li><i class="fa fa-phone"></i> Telepon alternatif : <a href="tel:0215703303">021 5703303</a> / <a href="tel:5711144">5711144</a> ext. 2115</li>
			    <li><i class="fa fa-envelope"></i> SMS : 0811976929</li>
			    <li><i class="fa fa-envelope"></i> Email : <a href="mailto:pengaduan@kemdikbud.go.id">pengaduan[at]kemdikbud.go.id</a></li>
			</ul>
			</p>
		    </div>
		    
		</td>
		<td width='33%' style='background-color:#008cd1' valign="top">
		    <div class="footer-widget" style='margin-left:30px; color:black; margin-top:10px'>
			<h4 class="footer-widget-title">Kunjungi Kami di Sosial Media</h4>
			<ul class="footer-media-icons">
			    <li><a target="_blank" href="http://www.facebook.com/Kemdikbud.RI" class="fa fa-facebook"></a></li>
			    <li><a target="_blank" href="https://twitter.com/Kemdikbud_RI" class="fa fa-twitter"></a></li>
			</ul>
		    </div>
		</td>
		<td width='33%' style='background-color:#006a9e' valign="top">
		    <div class="footer-widget" style='margin-left:30px; color:black; margin-top:10px'>
			<h4 class="footer-widget-title">Sebarkan Informasi Ini</h4>
			<span class='st_facebook_large' displayText='Facebook' st_title="Saya ikut melukis masa depan pendidikan Indonesia dengan memberi masukan untuk Kurikulum"></span>
			<span class='st_twitter_large' displayText='Tweet' st_title="Saya ikut melukis masa depan pendidikan Indonesia dengan memberi masukan untuk Kurikulum"></span>
			<span class='st_email_large' displayText='Email' st_title="Saya ikut melukis masa depan pendidikan Indonesia dengan memberi masukan untuk Kurikulum"></span>
			
			<script type="text/javascript">var switchTo5x=true;</script>
			<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
			<script type="text/javascript">stLight.options({publisher: "c3edc0c1-b82d-4095-91b1-b2867cb00b11", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		    </div>
		</td>
	    </tr>
	</table>
	
	
    </div>
